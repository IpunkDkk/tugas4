import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Depan from '../pages/welcome';
import Belakang from '../pages/home';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
            name = "Splash"
            component = {Depan}
            option = {{HeaderShown : false}}
            />
        <Stack.Screen 
            name = "Home"
            component = {Belakang}
            option = {{HeaderShown : false}}
            />
        </Stack.Navigator>
    );
};

export default Route;