import React , {useEffect} from 'react';
import { View , Image , StyleSheet , Text} from 'react-native';
import Wel from '../../icons/png/welcome.png';

const Welcome = () => {
useEffect(()=>{
    setTimeout(()=>{
        navigation.replace('Home');
    }, 5000)
});
    return (
    <View style={styles.container}>
        <Image
            style={styles.logo}
            source={Wel}
        />
        <Text styles={styles.font}>To My APP</Text>
    </View>
    );
};
    
    const styles = StyleSheet.create({
      container: {
        justifyContent: 'center',
        alignItems: 'center',
      },
      logo: {
        width: 300,
        height: 400,
      },
      font: {
          fontSize: 40 ,
          fontWeight: 'bold' ,
      }
    });

export default Welcome;